# Checklist of TODO's

[ ] Refactor sever (controllers and helpers)
[ ] Refactor sever startup to init
[ ] Implement server logging
[ ] Check externalities/env are available and working before server start
[ ] Tests for /ouath_request
[ ] Tests for /connect
[ ] Tests for /tweets
[ ] Tests for /disconnect
[ ] Implement /tweets
[ ] Implement /disconnect

[ ] Move webpack dev server to node api and serve static files from express
[ ] Update README with install/build instructions
[ ] Frontend should immediately call connect when twitter calls back with ouath
[ ] Cache results of POST /connect in redux store (keeps signed auth token)
[ ] Persist the redux store (redux-persist)
[ ] Create TweetList component
[ ] Create RefreshTweets component
[ ] Create LogoutButton component
[ ] Style pages
[ ] Write shallow render tests for all UI components
