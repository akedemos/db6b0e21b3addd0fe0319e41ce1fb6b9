import React from 'react'
// import { compose } from 'recompose'
// import { connect } from 'react-redux'

import TwitterLoginButton from '../components/twitter-login-button'

export default () => (
  <div>
    <TwitterLoginButton requestTokenUrl='http://127.0.0.1:3001/oauth_request' />
  </div>
)
