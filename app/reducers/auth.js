import { AUTHENTICATE } from '../actions'

const authReducer = (initialState = null, action) => {
  switch (action.type) {
    case AUTHENTICATE:
      return action.payload
    default:
      return initialState
  }
}

export default authReducer
