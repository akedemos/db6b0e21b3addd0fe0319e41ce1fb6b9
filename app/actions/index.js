
export const AUTHENTICATE = 'AUTHENTICATE'

export const authenticate = (authData) => ({
  type: AUTHENTICATE,
  payload: authData
})
