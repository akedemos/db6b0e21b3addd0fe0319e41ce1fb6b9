import React from 'react'
import withAuth from './components/withAuth'
import { withRouter } from 'react-router'
import { Route } from 'react-router-dom'
import { compose, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import LoginPage from './pages/login'
import TweetsPage from './pages/tweets'
import { authenticate } from './actions'
import qs from 'querystring'

export default withRouter(compose(
  connect(null, (dispatch) => ({
    authenticate: (authData) => dispatch(authenticate(authData))
  })),
  withAuth(),
  lifecycle({
    componentWillMount: function () {
      const { location, authed, authenticate, history } = this.props
      // if this is the auth callback then lets authenticate the user
      if (location.pathname === '/connect/twitter/callback') {
        let authData = qs.parse(location.search.substring(1))
        authenticate(authData)
        return history.push('/tweets')
      }

      // if the user is not authedredirect and show them the login page
      if (!authed && location.pathname !== '/login') {
        return history.push('/login')
      }
    }
  })
)(() => {
  return (
    <div>
      <div>
        <Route exact path='/login' component={LoginPage} />
        <Route exact path='/tweets' component={TweetsPage} />
      </div>
    </div>
  )
}))
