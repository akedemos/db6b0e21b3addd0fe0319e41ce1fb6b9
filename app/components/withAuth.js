import { compose } from 'recompose'
import { connect } from 'react-redux'

export default () => compose(
  connect(
    // mapState
    (state) => ({ authed: !!state.auth })
  )
)
