import React from 'react'
import PropTypes from 'prop-types'
import { compose, setPropTypes, setDisplayName, withHandlers } from 'recompose'
import { Button, Donut, Flex, Box, Text } from 'rebass'
import 'whatwg-fetch'
import qs from 'querystring'

export default compose(
  setDisplayName('TwitterLoginButton'),
  setPropTypes({
    requestTokenUrl: PropTypes.string.isRequired
  }),
  withHandlers({
    onGetRequestToken: ({ requestTokenUrl }) => () => {
      fetch(requestTokenUrl, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(authData => {
          let redirectUri = 'https://api.twitter.com/oauth/authenticate' + '?' + qs.stringify({ oauth_token: authData.oauth_token })
          window.location.replace(redirectUri)
        })
    }
  })
)(({ onGetRequestToken }) => (
  <Button onClick={onGetRequestToken}>
    <Flex justify='center' align='center'>
      <Box><Text>Sign in with Twitter</Text></Box>
      <Box px={10}><Donut size={20} value={1} strokeWidth={3} color='white' /></Box>
    </Flex>
  </Button>
))
