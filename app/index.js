import ReactDOM from 'react-dom'
import React from 'react'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { Provider as RebassProvider } from 'rebass'
import { Provider as ReduxProvider } from 'react-redux'
import { routerReducer, ConnectedRouter } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'
import logger from 'redux-logger'
import authReducer from './reducers/auth'

import App from './App'

export const history = createHistory()

// Add the reducer to your store on the `routing` key
const store = createStore(
  combineReducers({
    routing: routerReducer,
    auth: authReducer
  }),
  applyMiddleware(logger)
)

ReactDOM.render(
  <RebassProvider>
    <ReduxProvider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </ReduxProvider>
  </RebassProvider>,
  document.getElementById('app')
)
