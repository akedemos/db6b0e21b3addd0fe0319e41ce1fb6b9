require('dotenv').config()

const {
  PORT,
  TWITTER_CONSUMER_KEY,
  TWITTER_CONSUMER_SECRET,
  FIREBASE_KEY,
  FIREBASE_DATABASE_URL
} = process.env

const express = require('express')
const request = require('request')
const qs = require('qs')
const cors = require('cors')
const bodyParser = require('body-parser')
const firebase = require('firebase')
const async = require('async')
const app = express()

// Initialize Firebase
var config = {
  apiKey: FIREBASE_KEY,
  databaseURL: FIREBASE_DATABASE_URL
}
firebase.initializeApp(config)
const database = firebase.database()

// enable cors
var corsOption = {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token']
}
app.use(cors(corsOption))

// rest API requirements
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())

const defaultOauth = {
  consumer_key: TWITTER_CONSUMER_KEY,
  consumer_secret: TWITTER_CONSUMER_SECRET
}

app.get('/oauth_request', (req, res) => {
  request.post({
    url: 'https://api.twitter.com/oauth/request_token',
    oauth: defaultOauth
  }, (err, r, body) => {
    if (err) {
      return res.status(500).send({ message: err.message })
    }

    var data = qs.parse(body)
    res.json(data)
  })
})

app.post('/connect', (req, res) => {
  // TODO: check for the users information in the database

  const { oauth_token, oauth_verifier } = req.body
  if (!oauth_token) { // eslint-disable-line camelcase
    return res.status(400).send({ message: 'ouath_token is required' })
  }
  if (!oauth_verifier) { // eslint-disable-line camelcase
    return res.status(400).send({ message: 'oauth_verifier is required' })
  }

  async.waterfall([
    // gather the access token from twitter using the users token
    (cb) => {
      let oauth = Object.assign({}, defaultOauth, { token: oauth_token, verifier: oauth_verifier })
      request.post(
        'https://api.twitter.com/oauth/access_token',
        { oauth },
        (err, r, body) => cb(err, body)
      )
    },
    // use the new token to get the users profile
    (body, cb) => {
      let userAuth = qs.parse(body)
      let oauth = Object.assign({}, defaultOauth, {
        token: userAuth.oauth_token,
        token_secret: userAuth.oauth_token_secret
      })
      let query = {
        screen_name: userAuth.screen_name,
        user_id: userAuth.user_id
      }
      request.get(
        'https://api.twitter.com/1.1/users/show.json',
        { oauth, qs: query },
        (err, r, body) => cb(err, userAuth, body)
      )
    },
    // save the users profile to the database
    (userAuth, body, cb) => {
      try {
        let user = JSON.parse(body)
        user.auth = userAuth
        database.ref(`users/${userAuth.user_id}`).set(user, (err) => {
          cb(err, user)
        })
      } catch (err) {
        cb(err)
      }
    }
  ], (err, results) => {
    if (err) {
      res.status(500).send({ message: err })
    }
    res.send(results)
  })
})

app.get('/tweets', (req, res) => res.send('Not yet implemented'))

app.post('/disconnect', (req, res) => res.send('Not yet implemented'))

app.listen(PORT, function () {
  console.log(`Example app listening on port ${PORT}!`)
})
